package com.gaming.hutts.huttscentral;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.gaming.hutts.tasks.DataBinder;
import com.gaming.hutts.tasks.IStartupDownloadDataTask;
import com.gaming.hutts.web.WebUtils;
import com.wang.avi.AVLoadingIndicatorView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SplashActivity extends AppCompatActivity implements IStartupDownloadDataTask {
    private Unbinder unbinder;
    @BindView(R.id.loadAnimation) AVLoadingIndicatorView animation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);
        unbinder = ButterKnife.bind(this);

        if(animation != null)
            animation.show();

        if(WebUtils.deviceHasNetworkConnection())
            new DownloaderTask(this).execute();
        else
            noConectionAlert();
    }

    public void onSuccess() {
        startMainActivity();
    }

    public void onError() {
        new AlertDialog.Builder(this)
                .setTitle("Error")
                .setMessage("There has been an error downloading necessary data")
                .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startMainActivity();
                    }
                })
                .show();
    }

    private void noConectionAlert() {
        new AlertDialog.Builder(this)
                .setTitle("No Internet")
                .setMessage("Could not find any active Internet connection. Will not be able to download data!")
                .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startMainActivity();
                    }
                })
                .show();
    }

    private void startMainActivity() {
        Intent i = new Intent(this, MainMenuActivity.class);
        startActivity(i);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(unbinder != null)
            unbinder.unbind();

        if(animation != null)
            animation.hide();
    }

    /*
     * async task to download the data
     * there are callbacks to SplashActivity to take care of result states
     */
    private class DownloaderTask extends AsyncTask<String, Void, Void> {

        private IStartupDownloadDataTask callback;

        private DownloaderTask(IStartupDownloadDataTask task) {
            callback = task;
        }

        @Override
        protected Void doInBackground(String... strings) {
            Log.d("SplashActivity", "Starting download of data");
            boolean isOk = DataBinder.getInstance().downloadDataOnStartup();
            if(isOk) {
                callback.onSuccess();
                Log.d("SplashActivity", "Download completed successfully");
            } else {
                callback.onError();
                Log.d("SplashActivity", "Download completed with errors");
            }

            return null;
        }
    }
}
