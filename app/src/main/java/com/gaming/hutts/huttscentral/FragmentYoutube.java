package com.gaming.hutts.huttscentral;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.gaming.hutts.gui.YouTubeVideoListFragment;
import com.gaming.hutts.tasks.YouTubeVideoGuiTask;
import com.google.android.youtube.player.YouTubeApiServiceUtil;
import com.google.android.youtube.player.YouTubeInitializationResult;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Martin on 5. 7. 2016.
 */
public class FragmentYoutube extends BaseFragment {
    private final String APINAME = "huttscentralkey";
    /* API KEY */
    public static final String APIKEY = "AIzaSyBGwJdSHqxnqpBGJ_hzNOhFTMQW1fIMyK4";
    /* The request code when calling startActivityForResult to recover from an API service error. */
    private static final int RECOVERY_DIALOG_REQUEST = 1;

    private final int NUM_TABS = 2;
    private YouTubeTabPagerAdapter pagerAdapter;

    @BindView(R.id.youtubepager) ViewPager viewPager;
    @BindView(R.id.youtube_tabs) TabLayout tabLayout;


    //TODO: Search button in Actionbar
    //TODO: additional results based on date
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle bundle) {
        setActionBarTitle(R.string.fragment_youtube_title);
        View v = inflater.inflate(R.layout.fragment_youtube_layout, viewGroup, false);
        unbinder = ButterKnife.bind(this, v);

        pagerAdapter = new YouTubeTabPagerAdapter(getFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        checkYouTubeAPI();

        return v;
    }

    private void checkYouTubeAPI() {
        YouTubeInitializationResult errorReason =
                YouTubeApiServiceUtil.isYouTubeApiServiceAvailable(getContext());

        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(getActivity(), RECOVERY_DIALOG_REQUEST).show();
        } else if (errorReason != YouTubeInitializationResult.SUCCESS) {
            String errorMessage =
                    String.format(getString(R.string.youtube_error), errorReason.toString());
            Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            // Recreate the fragment if user performed a recovery action
            getFragmentManager().beginTransaction()
                    .replace(R.id.fragmentLayout, new FragmentYoutube(), FragmentYoutube.class.getSimpleName())
                    .disallowAddToBackStack()
                    .commit();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    /*
     * Pager Adapter implementation
     * holds different YouTube list fragments
     */
    private class YouTubeTabPagerAdapter extends FragmentPagerAdapter {

        private YouTubeVideoListFragment[] fragmentList;

        public YouTubeTabPagerAdapter(FragmentManager fm) {
            super(fm);
            fragmentList = new YouTubeVideoListFragment[NUM_TABS];
            for(int i = 0; i < NUM_TABS; i++) {
                fragmentList[i] = new YouTubeVideoListFragment();
                fragmentList[i].setTabNumber(i+1);
            }
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList[position];
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.tab_youtube_hutts);
                case 1:
                    return getResources().getString(R.string.tab_youtube_hutts2);
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return NUM_TABS;
        }
    }
}
