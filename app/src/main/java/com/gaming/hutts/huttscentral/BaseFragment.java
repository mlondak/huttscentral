package com.gaming.hutts.huttscentral;

import android.support.v4.app.Fragment;

import butterknife.Unbinder;

/**
 * Created by Martin on 18. 6. 2016.
 */
public class BaseFragment extends Fragment {
    protected Unbinder unbinder;

    protected void setActionBarTitle(int resId) {
        getActivity().setTitle(resId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(unbinder != null)
            unbinder.unbind();
    }
}
