package com.gaming.hutts.huttscentral;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Martin on 9. 7. 2016.
 * This is supposed to be a fullscreen landscape activity for playback of a YouTube video
 * ID of the video is passed in Intent extras
 */
public class YouTubeVideoActivity extends YouTubeBaseActivity {
    YouTubePlayerView playerView;
    YouTubePlayer player;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_video);
        playerView = (YouTubePlayerView) findViewById(R.id.youtubePlayerView);
        final String videoID = getIntent().getExtras().getString("youtubeVideoID");

        playerView.initialize(FragmentYoutube.APIKEY, new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                player = youTubePlayer;
                player.setFullscreen(true);
                player.setShowFullscreenButton(true);
                player.loadVideo(videoID);

                player.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
                    @Override
                    public void onLoading() {
                    }

                    @Override
                    public void onLoaded(String s) {
                    }

                    @Override
                    public void onAdStarted() {
                    }

                    @Override
                    public void onVideoStarted() {
                    }

                    @Override
                    public void onVideoEnded() {
                        Log.i("YouTube Video", "Playback of video ended.");
                    }

                    @Override
                    public void onError(YouTubePlayer.ErrorReason errorReason) {
                        Log.e("YouTube Video", "Error while loading/playing YouTube video: " + errorReason.toString());
                    }
                });
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                player = null;
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        //ignore configuration and orientation change
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onDestroy() {
        if(player != null)
            player.release();

        super.onDestroy();
    }
}
