package com.gaming.hutts.huttscentral;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.gaming.hutts.gui.MainMenuListAdapter;
import com.gaming.hutts.tasks.DataBinder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Martin on 6. 6. 2016.
 */
public class FragmentOverview extends BaseFragment {
    private List<String> headers;
    private HashMap<String, List<Object>> data;

    @BindView(R.id.newsListView) ExpandableListView list;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setActionBarTitle(R.string.drawer_title);
        View v = inflater.inflate(R.layout.fragment_overview_layout, container, false);

        unbinder = ButterKnife.bind(this, v);

        prepareData();
        MainMenuListAdapter listAdapter = new MainMenuListAdapter(this.getContext(), headers, data);
        list.setAdapter(listAdapter);

        return v;
    }

    private void prepareData() {
        headers = new ArrayList<>();
        data = new HashMap<>();

        // header data
        headers.add("Newest videos");
        headers.add("Newest tweets");
        headers.add("Newest quotes");

        //child data
        List<Object> videos = new ArrayList<>();

        List<Object> tweets = new ArrayList<>();
        tweets.addAll(DataBinder.getInstance().getTwitterFeedData(3));

        List<Object> quotes = new ArrayList<>();
        quotes.addAll(DataBinder.getInstance().getHuttssaysFeedData(3));

        data.put(headers.get(0), videos);
        data.put(headers.get(1), tweets);
        data.put(headers.get(2), quotes);
    }
}
