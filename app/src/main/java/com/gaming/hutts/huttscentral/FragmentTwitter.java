package com.gaming.hutts.huttscentral;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gaming.hutts.gui.DividerItemDecoration;
import com.gaming.hutts.gui.TweetAdapter;
import com.gaming.hutts.tasks.ITwitterTask;
import com.gaming.hutts.tasks.TwitterGuiTask;
import com.gaming.hutts.data.Tweet;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Martin on 6. 6. 2016.
 */
public class FragmentTwitter extends BaseFragment implements ITwitterTask {
    @BindView(R.id.tweet_recycler_view) RecyclerView recycler_view;

    // TODO: maybe change actionbar color to Twitter color?
    // TODO: URLs visible and clickable!
    // TODO: maybe add italics for each tweet handle in tweets?
    // TODO: add twitter logo to title
    // TODO: better date formatting?
    // TODO: persistence in DB, not loading tweets when orientation change
    // TODO: add space for the last item to be visible below the button
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setActionBarTitle(R.string.fragment_twitter_title);
        View v = inflater.inflate(R.layout.fragment_twitter_layout, container, false);
        unbinder = ButterKnife.bind(this, v);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recycler_view.setHasFixedSize(true);
        // use a linear layout manager
        recycler_view.setLayoutManager(new LinearLayoutManager(this.getContext()));
        //set divider decoration
        DividerItemDecoration decor = new DividerItemDecoration(
                ContextCompat.getDrawable(getContext(), R.drawable.divider_lightgrey));
        recycler_view.addItemDecoration(decor);
        new TwitterGuiTask(this).execute();

        return v;
    }

    public void populateRecyclerView(ArrayList<Tweet> list){
        if(recycler_view != null){
            recycler_view.setAdapter(new TweetAdapter(list));
        }
    }
}
