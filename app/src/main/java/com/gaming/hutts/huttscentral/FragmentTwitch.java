package com.gaming.hutts.huttscentral;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.gaming.hutts.web.HuttsCentralWebViewClient;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentTwitch extends BaseFragment {
    @BindView(R.id.twitch_view) WebView twitch_tv;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setActionBarTitle(R.string.fragment_twitch_title);
        View v = inflater.inflate(R.layout.fragment_twitch_layout, container, false);
        unbinder = ButterKnife.bind(this, v);

        WebSettings webSettings = twitch_tv.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);

        // Load the Twitch Stream you want
        // Actually, this is the most important line if code
        // The "embed" searches for the suitable stream format
        twitch_tv.setWebViewClient(new HuttsCentralWebViewClient());
        twitch_tv.loadUrl("http://www.twitch.tv/huttsgaming/hls");

        return v;
    }
}
