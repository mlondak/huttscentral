package com.gaming.hutts.huttscentral;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainMenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.fab) FloatingActionButton floatingActionButton;

    private final String FRAGMENT_STACK = "frag_stack";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);

        setFloatingActionButtonInvisible();
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment currentFrag = getSupportFragmentManager().findFragmentById(R.id.fragmentLayout);
                String tag = currentFrag.getTag();

                //switch expects constant expression... :(
                if(tag.equals(FragmentOverview.class.getSimpleName())) {
                    //nothing to do here
                }
                else if(tag.equals(FragmentTwitter.class.getSimpleName())){
                    //nothing to do here
                }
                else if(tag.equals(FragmentHuttssays.class.getSimpleName())){
                    // plus icon clicked
                    Toast.makeText(MainMenuActivity.this, "Plus icon clicked", Toast.LENGTH_LONG).show();
                }
                else if(tag.equals(FragmentYoutube.class.getSimpleName())) {
                    //nothing to do here
                }
                else if(tag.equals(FragmentTwitch.class.getSimpleName())) {
                    //nothing to do here
                }
                else {
                    Log.d("MainMenuActivity", "Missing switch statement case: " + tag);
                }
            }
        });

        if(savedInstanceState == null) {
            FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction()
                .add(R.id.fragmentLayout, new FragmentOverview(), FragmentOverview.class.getSimpleName())
                .disallowAddToBackStack()
                .commit();
            fm.executePendingTransactions();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        FragmentManager fm = getSupportFragmentManager();
        Fragment f = null;

        if (id == R.id.nav_home) {
            f = new FragmentOverview();
            setFloatingActionButtonInvisible();
        } else if (id == R.id.nav_youtube) {
            f = new FragmentYoutube();
            setFloatingActionButtonInvisible();
        } else if (id == R.id.nav_twitter) {
            f = new FragmentTwitter();
            setFloatingActionButtonInvisible();
        } else if (id == R.id.nav_huttssays) {
            f = new FragmentHuttssays();
            setFloatingActionButtonVisibilityAndImage(true, R.drawable.fb_plus);
        } else if (id == R.id.nav_twitch) {
            f = new FragmentTwitch();
            setFloatingActionButtonInvisible();
        }

        if(f != null) {
            fm.popBackStack(f.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fm.beginTransaction()
                    .replace(R.id.fragmentLayout, f, f.getClass().getSimpleName())
                    .addToBackStack(FRAGMENT_STACK)
                    .commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if(drawer != null)
            drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    private void setFloatingActionButtonInvisible() {
        setFloatingActionButtonVisibilityAndImage(false, 0);
    }

    private void setFloatingActionButtonVisibilityAndImage(boolean shouldBeVisible, final int resId) {
        if(!shouldBeVisible) {
            floatingActionButton.hide();
        }
        else {
            floatingActionButton.hide(new FloatingActionButton.OnVisibilityChangedListener() {
                @Override
                public void onHidden(FloatingActionButton fab) {
                    fab.setImageResource(resId);
                    fab.show();
                }
            });
        }
    }
}
