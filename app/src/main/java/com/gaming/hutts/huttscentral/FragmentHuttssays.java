package com.gaming.hutts.huttscentral;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gaming.hutts.data.HuttsSays;
import com.gaming.hutts.gui.DividerItemDecoration;
import com.gaming.hutts.gui.HuttssaysAdapter;
import com.gaming.hutts.gui.TweetAdapter;
import com.gaming.hutts.tasks.HuttssaysGuiTask;
import com.gaming.hutts.tasks.IHuttssaysTask;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Martin on 18. 6. 2016.
 */
public class FragmentHuttssays extends BaseFragment implements IHuttssaysTask {
    @BindView(R.id.hs_recycler_view) RecyclerView recycler_view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setActionBarTitle(R.string.fragment_huttssays_title);
        View v = inflater.inflate(R.layout.fragment_huttssays_layout, container, false);
        unbinder = ButterKnife.bind(this, v);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recycler_view.setHasFixedSize(true);
        // use a linear layout manager
        recycler_view.setLayoutManager(new LinearLayoutManager(this.getContext()));
        //set divider decoration

        DividerItemDecoration decor = new DividerItemDecoration(
                ContextCompat.getDrawable(getContext(), R.drawable.divider_lightgrey));
        recycler_view.addItemDecoration(decor);
        new HuttssaysGuiTask(this).execute();

        return v;
    }

    @Override
    public void populateRecyclerView(ArrayList<HuttsSays> result) {
        if(recycler_view != null){
            recycler_view.setAdapter(new HuttssaysAdapter(result));
        }
    }
}
