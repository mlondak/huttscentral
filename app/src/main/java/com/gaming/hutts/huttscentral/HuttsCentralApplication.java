package com.gaming.hutts.huttscentral;

import android.app.Application;
import android.content.Context;

/**
 * Created by Martin on 23. 6. 2016.
 */
public class HuttsCentralApplication extends Application {
    private static HuttsCentralApplication instance;

    //use for app-wide usage
    //use Activity Context in local and timely usages
    public static Context getAppContext() {
        return instance.getApplicationContext();
    }

    @Override
    public void onCreate() {
        instance = this;
        super.onCreate();
    }
}
