package com.gaming.hutts.web;

import android.webkit.WebView;
import android.webkit.WebViewClient;

public class HuttsCentralWebViewClient extends WebViewClient {
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        //if (url.contains("twitch.tv")) {
            // This is Hutts twitch site, so do not override; let my WebView load the page
            return false;
        //}

        //return super.shouldOverrideUrlLoading(view, url);
    }
}
