package com.gaming.hutts.web;

import android.content.Context;
import android.net.ConnectivityManager;

import com.gaming.hutts.huttscentral.HuttsCentralApplication;

public class WebUtils {

    public static boolean deviceHasNetworkConnection() {
        ConnectivityManager cm = (ConnectivityManager) HuttsCentralApplication.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
}
