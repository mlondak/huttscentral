package com.gaming.hutts.tasks;

import android.util.Log;

import org.apache.commons.io.IOUtils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by Martin on 18. 6. 2016.
 */
public class HttpConnectionTasks {
    public static HttpURLConnection createUrlConnection(String urlString, String requestMethod, HashMap<String, String> params, String additionalParam) {
        try {
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(requestMethod);

            if(params != null)
                setParameters(conn, params);

            if(additionalParam != null && !additionalParam.isEmpty())
                setAdditionalValue(conn, additionalParam);

            return conn;
        }
        catch(IOException ex) {
            Log.e("HttpConnectionTasks", "Error while setting up and starting connection: " + ex.getMessage());
        }

        return null;
    }

    private static HttpURLConnection setParameters(HttpURLConnection conn, HashMap<String, String> params) {
        for (String key : params.keySet()) {
            String value = params.get(key);
            conn.setRequestProperty(key, value);
        }
        return conn;
    }

    private static HttpURLConnection setAdditionalValue(HttpURLConnection conn, String object) {
        try {
            conn.setDoOutput(true);
            //conn.connect();

            BufferedOutputStream stream = new BufferedOutputStream(conn.getOutputStream());
            stream.write(object.getBytes());
            stream.flush();
        }
        catch (IOException ex){
            Log.e("HttpConnectionTasks", "Error while setting additional params: " + ex.getMessage());
        }

        return conn;
    }

    public static String getResponseBody(HttpURLConnection conn)
    {
        InputStream in = null;
        String response = null;
        try {
            conn.connect();
            // read the response
            Log.d("TwitterGuiTask", "The response is: " + response);
            in = new BufferedInputStream(conn.getInputStream());
            response = IOUtils.toString(in, "UTF-8");
            conn.disconnect();
        } catch(IOException e){
            Log.e("TwitterGuiTask", "Error getting a response: " + e.getMessage());
        }
        finally {
            try {
                if (in != null)
                    in.close();
            } catch(IOException e) {}
        }

        return response;
    }
}
