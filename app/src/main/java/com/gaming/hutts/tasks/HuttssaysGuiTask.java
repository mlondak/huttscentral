package com.gaming.hutts.tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.gaming.hutts.data.HuttsSays;
import com.gaming.hutts.data.Tweet;

import java.util.ArrayList;

/**
* Created by Martin on 18. 6. 2016.
*/
public class HuttssaysGuiTask extends AsyncTask<String, Void, ArrayList<HuttsSays>> {
    //get Huttssays quotes async task

    private IHuttssaysTask _task;

    public HuttssaysGuiTask(IHuttssaysTask task) {
        this._task = task;
    }

    @Override
    protected ArrayList<HuttsSays> doInBackground(String... strings) {
        return DataBinder.getInstance().getHuttssaysFeedData(20);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(ArrayList<HuttsSays> result) {
        if(result != null)
            _task.populateRecyclerView(result);
        else
            Log.d("HuttsaysGuiTask", "No quotes could be got at this time");
    }
}
