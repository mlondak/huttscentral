package com.gaming.hutts.tasks;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import com.gaming.hutts.data.HuttsSays;
import com.gaming.hutts.data.Tweet;
import com.gaming.hutts.database.DatabaseContract;
import com.gaming.hutts.database.DatabaseHelper;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Martin on 19. 6. 2016.
 */
// This class should be giving data to whoever asks for them
// and should abstract from higher classes knowing whether data
// come directly from Internet or DB
public class DataBinder {
    private static DataBinder instance;
    private static ExecutorService executorService;
    private DatabaseHelper dbh;

    public static synchronized DataBinder getInstance() {
        if(instance == null)
            return new DataBinder();
        return instance;
    }

    private DataBinder () {
        dbh = DatabaseHelper.getInstance();
        instance = this;
    }

    @Nullable
    public ArrayList<Tweet> getTwitterFeedData(int noOfRows) {
        if(!dbh.doDataExistInDb(DatabaseContract.TwitterTable.TABLE_NAME)) {
            boolean finished = false;
            executorService = Executors.newSingleThreadExecutor();
            executorService.execute(new RunnableTwitter());
            executorService.shutdown();
            try {
                 finished = executorService.awaitTermination(1, TimeUnit.MINUTES);
            }
            catch(InterruptedException ex) {
                Log.e("DataBinder", "Error while awaiting Twitter thread termination: " + ex.getMessage());
            }
            finally {
                Log.d("DataBinder", "Termination of Twitter thread result: " + finished);
            }
        }

        return dbh.getTwitterRows(noOfRows);
    }

    public ArrayList<HuttsSays> getHuttssaysFeedData(int noOfRows) {
        if(!dbh.doDataExistInDb(DatabaseContract.HuttssaysTable.TABLE_NAME)) {
            boolean finished = false;
            executorService = Executors.newSingleThreadExecutor();
            executorService.execute(new RunnableHuttssays(20));
            executorService.shutdown();
            try {
                finished = executorService.awaitTermination(1, TimeUnit.MINUTES);
            }
            catch(InterruptedException ex) {
                Log.e("DataBinder", "Error while awaiting Huttssays thread termination: " + ex.getMessage());
            }
            finally {
                Log.d("DataBinder", "Termination of Huttssays thread result: " + finished);
            }
        }

        return dbh.getHuttssaysRows(noOfRows);
    }

    public boolean downloadDataOnStartup() {
        boolean finished = false;
        executorService = Executors.newFixedThreadPool(2);
        executorService.execute(new RunnableHuttssays(40));
        executorService.execute(new RunnableTwitter());

        executorService.shutdown();
        try {
            finished = executorService.awaitTermination(1, TimeUnit.MINUTES);
        }
        catch(InterruptedException ex) {
            Log.e("DataBinder", "Error while downloading startup data thread termination: " + ex.getMessage());
            return false;
        }
        finally {
            Log.d("DataBinder", "Termination of downloading startup data threads, result: " + finished);
        }

        return true;
    }
}
