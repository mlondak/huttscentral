package com.gaming.hutts.tasks;

import com.gaming.hutts.data.HuttsSays;

import java.util.ArrayList;

/**
 * Created by Martin on 26. 6. 2016.
 */
public interface IHuttssaysTask {
    void populateRecyclerView(ArrayList<HuttsSays> result);
}
