package com.gaming.hutts.tasks;

import com.gaming.hutts.data.YouTubeVideo;

import java.util.ArrayList;

public interface IYouTubeVideoTask {
    void addItemToAdapter(YouTubeVideo video);
    void addItemRangeToAdapter(ArrayList<YouTubeVideo> videoList);
    String getHuttsChannelId();
}
