package com.gaming.hutts.tasks;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.gaming.hutts.data.Tweet;
import com.gaming.hutts.data.TwitterConst;
import com.gaming.hutts.database.DatabaseHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Martin on 19. 6. 2016.
 */
public class RunnableTwitter implements Runnable {

    @Override
    public void run() {
        String jsonResponse = getTwitterStream();
        ArrayList<Tweet> tweetList = parseTweetsFromJson(jsonResponse);
        DatabaseHelper.getInstance().insertBulkTwitterRows(tweetList);
    }

    private String getTwitterStream()
    {
        Tweet latestTweet = DatabaseHelper.getInstance().getMostRecentTweet();

        try {
            //encode token and get a token
            String urlApiKey = URLEncoder.encode(TwitterConst.CONSUMER_KEY, "UTF-8");
            String urlApiSecret = URLEncoder.encode(TwitterConst.CONSUMER_SECRET, "UTF-8");

            // Concatenate the encoded consumer key, a colon character, and the
            // encoded consumer secret
            String combined = urlApiKey + ":" + urlApiSecret;

            // Base64 encode the string
            String base64Encoded = Base64.encodeToString(combined.getBytes(), Base64.NO_WRAP);

            HashMap<String, String> connParams = new HashMap<String, String>();
            connParams.put("Authorization", "Basic " + base64Encoded);
            connParams.put("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

            HttpURLConnection postConn = HttpConnectionTasks.createUrlConnection(
                    TwitterConst.TwitterTokenURL,
                    "POST",
                    connParams,
                    "grant_type=client_credentials"
            );

            String rawAuthorization = HttpConnectionTasks.getResponseBody(postConn);
            JSONObject auth = new JSONObject(rawAuthorization);

            // Applications should verify that the value associated with the
            // token_type key of the returned object is bearer
            if (auth.getString("token_type").equals("bearer")) {

                connParams.clear();
                connParams.put("Authorization", "Bearer " + auth.getString("access_token"));
                connParams.put("Content-Type", "application/json");

                String url = TwitterConst.TwitterStreamURL;
                if(latestTweet != null && !latestTweet.isMoreThanWeekOld()) {
                    url += "&count=" + 100;
                    url += "&since_id=" + latestTweet.getId();
                }

                // Authenticate API requests with bearer token
                HttpURLConnection getConn = HttpConnectionTasks.createUrlConnection(
                        url,
                        "GET",
                        connParams,
                        null
                );

                // update the results with the body of the response
                return HttpConnectionTasks.getResponseBody(getConn);
            }
        } catch (Exception e) {
            Log.e("TwitterGuiTask", "Error while authenticating and getting tweets: " + e.getMessage());
            return null;
        }

        return null;
    }

    private ArrayList<Tweet> parseTweetsFromJson(String jsonString)
    {
        ArrayList<Tweet> tweets = new ArrayList<>();

        try {
            JSONArray array = new JSONArray(jsonString);
            JSONObject jObject;

            for(int i = 0; i < array.length(); i++)
            {
                jObject = array.getJSONObject(i);
                Tweet tweet = new Tweet();

                tweet.setId(jObject.getString("id"));
                tweet.setName(jObject.getJSONObject("user").getString("name"));
                tweet.setUserName(jObject.getJSONObject("user").getString("screen_name"));
                tweet.setMessage(jObject.getString("text"));
                tweet.setDate(jObject.getString("created_at"));

                //replace _normal with _bigger to get bigger profile pic
                String imageUrl = jObject.getJSONObject("user").getString("profile_image_url");
                imageUrl = imageUrl.replace("_normal", "");
                tweet.setProfileImageUrl(imageUrl);

                tweets.add(tweet);
            }
        }
        catch (Exception e) {
            Log.e("Parsing tweets", "Error while parsing tweets: " + e.getMessage());
        }

        Log.e("Parsing tweets", "Downloaded: " + tweets.size() + " tweets");
        return tweets;
    }
}
