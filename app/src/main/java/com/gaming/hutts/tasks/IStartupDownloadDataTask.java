package com.gaming.hutts.tasks;

public interface IStartupDownloadDataTask {
    void onSuccess();
    void onError();
}
