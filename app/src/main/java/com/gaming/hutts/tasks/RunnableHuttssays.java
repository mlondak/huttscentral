package com.gaming.hutts.tasks;

import android.util.Log;

import com.gaming.hutts.data.HuttsSays;
import com.gaming.hutts.database.DatabaseHelper;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Martin on 23. 6. 2016.
 */
public class RunnableHuttssays implements Runnable {
    private final String HUTTSSAYS_URL = "https://huttssays.com/?page=";
    private final int QUOTES_PER_PAGE = 8;

    private int noOfQuotes = -1;

    public RunnableHuttssays(int numberOfQuotesToGet) {
        noOfQuotes = numberOfQuotesToGet;
    }

    @Override
    public void run() {
        try {
            ArrayList<HuttsSays> listOfQuotes = new ArrayList<HuttsSays>();
            int noOfPages = (int)Math.ceil(noOfQuotes / 8);
            for(int page = 1; page <= noOfPages; page++) {
                Document doc = Jsoup.connect(HUTTSSAYS_URL + page).get();
                listOfQuotes.addAll(parseHtmlDocument(doc));
            }

            if(listOfQuotes.size() > 0) {
                DatabaseHelper.getInstance().insertBulkHuttssaysRows(listOfQuotes);
            } else {
                Log.d("Huttssays Runnable", "No quotes downloaded");
            }
        } catch(IOException e) {
            Log.e("Huttssays runnable", "Error while getting and parsing data: " + e.getMessage());
        }
    }

    private ArrayList<HuttsSays> parseHtmlDocument(Document doc) {
        //prepare what we will need
        ArrayList<HuttsSays> list = new ArrayList<HuttsSays>();
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        try {
            //get HTML body
            Element body = doc.body();

            //get respective HTML elements
            Elements id_elements = body.select("a").select(".uk-margin");
            Elements quote_elements = body.select("div.uk-article").select(".uk-link");
            Elements sig_elements = body.select("div.uk-article").select(".uk-article-lead");

            //they should be of the same size
            if (id_elements.size() != quote_elements.size() ||
                id_elements.size() != sig_elements.size() ||
                quote_elements.size() != sig_elements.size()) {

                Log.e("Runnable Huttssays", "Element arrays sizes do not match! ID elements count : "
                        + id_elements.size() + ", quote elements count: " + quote_elements.size());
            }

            for (int i = 0; i < quote_elements.size(); i++) {
                HuttsSays hs = new HuttsSays();
                hs.setWebId(id_elements.get(i).attr("id"));
                Element content = quote_elements.get(i);
                hs.setUrlLink(content.select("a").first().attr("href"));
                hs.setTitle(content.select("a").first().text());
                Element sig = sig_elements.get(i);
                hs.setSignature(sig.text());
                hs.setDate(sdf.format(cal.getTime()));

                list.add(hs);
            }
        }
        catch (Exception ex) {
            Log.e("Runnable Huttssays", "Error in parseHtmlDocument(): " + ex);
            return new ArrayList<>();
        }

        return list;
    }
}
