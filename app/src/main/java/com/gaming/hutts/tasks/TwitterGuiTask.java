package com.gaming.hutts.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.gaming.hutts.data.Tweet;

import java.util.ArrayList;

/**
* Created by Martin on 18. 6. 2016.
*/
public class TwitterGuiTask extends AsyncTask<String, Void, ArrayList<Tweet>> {
    //get Tweets async task

    private ITwitterTask _task;

    public TwitterGuiTask(ITwitterTask task) {
        this._task = task;
    }

    @Override
    protected ArrayList<Tweet> doInBackground(String... strings) {
        return DataBinder.getInstance().getTwitterFeedData(20);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(ArrayList<Tweet> result) {
        if(result != null)
            _task.populateRecyclerView(result);
        else
            Log.d("TwitterGuiTask", "No tweets could be got at this time");
    }
}
