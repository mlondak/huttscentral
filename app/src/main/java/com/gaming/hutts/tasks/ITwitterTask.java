package com.gaming.hutts.tasks;

import com.gaming.hutts.data.Tweet;

import java.util.ArrayList;

/**
 * Created by Martin on 18. 6. 2016.
 */
public interface ITwitterTask {
    void populateRecyclerView(ArrayList<Tweet> result);
}
