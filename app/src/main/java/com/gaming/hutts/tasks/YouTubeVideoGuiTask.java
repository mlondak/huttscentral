package com.gaming.hutts.tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.gaming.hutts.data.YouTubeVideo;
import com.gaming.hutts.huttscentral.FragmentYoutube;
import com.gaming.hutts.huttscentral.HuttsCentralApplication;
import com.gaming.hutts.huttscentral.R;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;
import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoListResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class YouTubeVideoGuiTask extends AsyncTask<String, Void, ArrayList<YouTubeVideo>> {
    private final long MAX_VIDS = 20;

    private IYouTubeVideoTask task;
    private DateTime dateTimeConstraint;

    private YouTube youtube;
    private YouTube.Search.List query;

    public YouTubeVideoGuiTask(IYouTubeVideoTask callback) {
        task = callback;
    }

    //constructor to be used to retrieve additional data
    //dateTime to be the one from the last item in the current list
    public YouTubeVideoGuiTask(IYouTubeVideoTask callback, DateTime dateTimeFrom) {
        task = callback;
        dateTimeConstraint = dateTimeFrom;
    }

    @Override
    protected ArrayList<YouTubeVideo> doInBackground(String... strings) {
        if (youtube == null || query == null)
            return null;

        ArrayList<YouTubeVideo> list = new ArrayList<YouTubeVideo>();

        try {
            SearchListResponse searchResponse = query.execute();
            //we got result list of video IDs
            List<SearchResult> resultList = searchResponse.getItems();

            //based on videos IDs we make a call to videos.list
            //to get detailed video info
            YouTube.Videos.List videoQuery = youtube.videos().list("snippet,contentDetails,statistics");
            videoQuery.setKey(FragmentYoutube.APIKEY);
            videoQuery.setFields("items(id,snippet/publishedAt,snippet/title,contentDetails/duration,statistics/viewCount)");

            StringBuilder vidIds = new StringBuilder();
            if (resultList != null && resultList.size() > 0) {
                for (SearchResult result : resultList) {
                    vidIds.append(result.getId().getVideoId());
                    vidIds.append(",");
                }
                //delete trailing comma
                vidIds.deleteCharAt(vidIds.length() - 1);
            } else
                return list;

            videoQuery.setId(vidIds.toString());

            //execute the command
            VideoListResponse videoResponse = videoQuery.execute();
            List<Video> videosList = videoResponse.getItems();

            if (videosList != null && videosList.size() > 0) {
                for (Video vid : videosList) {
                    list.add(new YouTubeVideo(
                            vid.getSnippet().getTitle(),
                            vid.getId(),
                            vid.getSnippet().getPublishedAt(),
                            vid.getContentDetails().getDuration(),
                            vid.getStatistics().getViewCount()
                            )
                    );
                }
            }

        }catch(GoogleJsonResponseException e){
            System.err.println("There was a service error: " + e.getDetails().getCode() + " : "
                    + e.getDetails().getMessage());
            Log.e("YouTube task execute", "There was a service error: " + e.getDetails().getCode() + " : "
                    + e.getDetails().getMessage());
        }catch(IOException e){
            System.err.println("There was an IO error: " + e.getCause() + " : " + e.getMessage());
            Log.e("YouTube task execute", "There was an IO error: " + e.getCause() + " : " + e.getMessage());
        }
        return list;
    }

        @Override
        protected void onPreExecute() {
        try {
            //create instance of YouTube API caller thingy
            youtube = new YouTube.Builder(new NetHttpTransport(), new JacksonFactory(), new HttpRequestInitializer() {
                @Override
                public void initialize(HttpRequest request) throws IOException {
                }
            }).setApplicationName(HuttsCentralApplication.getAppContext().getString(R.string.app_name)).build();

            //create query request object and fill it with data
            query = youtube.search().list("id");
            query.setKey(FragmentYoutube.APIKEY);
            query.setType("video");
            query.setChannelId(task.getHuttsChannelId());
            query.setMaxResults(MAX_VIDS);
            query.setOrder("date");
            if (dateTimeConstraint != null)
                query.setPublishedBefore(dateTimeConstraint);

            //retrieve only fields we will need
            query.setFields("items(id/videoId)");
        } catch(IOException e) {
            Log.e("YouTube task preexecute", "There was an IO error getting video IDs: " + e.getCause() + " : " + e.getMessage());
            query = null;
            System.err.println(e);
        }
    }

    @Override
    protected void onPostExecute(ArrayList<YouTubeVideo> result) {
        if(result != null)
            task.addItemRangeToAdapter(result);
        else
            Log.d("YouTubeVideoGuiTask", "No youtube videos could be downloaded");
    }
}
