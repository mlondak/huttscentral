package com.gaming.hutts.gui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

import com.gaming.hutts.data.YouTubeVideo;
import com.gaming.hutts.huttscentral.YouTubeVideoActivity;
import com.gaming.hutts.tasks.IYouTubeVideoTask;
import com.gaming.hutts.tasks.YouTubeVideoGuiTask;

import java.util.ArrayList;
import java.util.List;

//this fragment shows list of YT videos in FragmentYouTube
public class YouTubeVideoListFragment extends ListFragment implements IYouTubeVideoTask {

    private final String HUTTS_CHANNEL_ID = "UCy3avhfHpBbbgwZpNvuVklg";
    private final String HUTTS2_CHANNEL_ID = "UC2HZ6A_jScWdijaeKWwQTaw";

    private List<YouTubeVideo> youTubeVideoList;
    private YouTubeVideoAdapter adapter;
    private int tabNumber = -1;

    //always set Tab number, best right after creating fragment
    //otherwise value of -1 will cancel all API calls
    public void setTabNumber(int num) {
        tabNumber = num;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        youTubeVideoList = new ArrayList<>();
        adapter = new YouTubeVideoAdapter(getActivity(), youTubeVideoList);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getListView().setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        setListAdapter(adapter);

        new YouTubeVideoGuiTask(this).execute();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        String videoId = youTubeVideoList.get(position).getVideoId()  ;

        Intent i = new Intent(getContext(), YouTubeVideoActivity.class);
        i.putExtra("youtubeVideoID", videoId);
        startActivity(i);
    }

    public void addItemToAdapter(YouTubeVideo video) {
        youTubeVideoList.add(video);
        adapter.notifyDataSetChanged();
    }

    public void addItemRangeToAdapter(ArrayList<YouTubeVideo> videoList) {
        youTubeVideoList.addAll(videoList);
        adapter.notifyDataSetChanged();
    }

    public String getHuttsChannelId(){
        if(tabNumber == -1) {
            Log.e("YouTubeVideoListFragmen", "YouTubeVideoListFragment was not initialized with tab number!");
            throw new IllegalStateException("YouTubeVideoListFragment was not initialized with tab number!");
        }

        switch(tabNumber){
            case 1:
                return HUTTS_CHANNEL_ID;
            case 2:
                return HUTTS2_CHANNEL_ID;
            default:
                return null;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        adapter.releaseLoaders();
    }
}
