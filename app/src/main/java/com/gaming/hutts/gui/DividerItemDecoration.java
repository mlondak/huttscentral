package com.gaming.hutts.gui;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Martin on 17. 6. 2016.
 */
public class DividerItemDecoration extends RecyclerView.ItemDecoration {
    private Drawable _divider;

    public DividerItemDecoration(Drawable divider) {
        this._divider = divider;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        //ignore item at position 0
        //it is easier than to look for last item
        if (parent.getChildAdapterPosition(view) == 0) {
            return;
        }

        //returning offset on top of an item
        outRect.top = _divider.getIntrinsicHeight();
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int dividerStartLeft = parent.getPaddingLeft();
        int dividerEndRight = parent.getWidth() - parent.getPaddingRight();

        int childCount = parent.getChildCount();

        for(int i = 0; i < childCount - 1; i++) {
            View child = parent.getChildAt(i);
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int dividerStartTop = child.getBottom() + params.bottomMargin;
            int dividerEndBottom = dividerStartTop + _divider.getIntrinsicHeight();

            _divider.setBounds(dividerStartLeft, dividerStartTop, dividerEndRight, dividerEndBottom);
            _divider.draw(c);
        }
    }
}
