package com.gaming.hutts.gui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gaming.hutts.data.HuttsSays;
import com.gaming.hutts.huttscentral.R;

import java.util.ArrayList;

/**
 * Created by Martin on 28. 6. 2016.
 */
public class HuttssaysAdapter extends RecyclerView.Adapter<HuttssaysViewHolder> {

    private ArrayList<HuttsSays> quotesList;

    public HuttssaysAdapter(ArrayList<HuttsSays> list) {
        this.quotesList = list;
    }

    @Override
    public HuttssaysViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.huttssays_item, parent, false);
        return new HuttssaysViewHolder(v);
    }

    @Override
    public void onBindViewHolder(HuttssaysViewHolder holder, int position) {
        holder.title.setText(quotesList.get(position).getTitle());
        holder.signature.setText(quotesList.get(position).getSignature());
        holder.setLink(quotesList.get(position).getUrlLink());
    }

    @Override
    public int getItemCount() {
        //number of elements
        return quotesList.size();
    }
}
