package com.gaming.hutts.gui;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.gaming.hutts.huttscentral.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Martin on 28. 6. 2016.
 */
public class HuttssaysViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.hs_title_tv) TextView title;
    @BindView(R.id.hs_signature_tv) TextView signature;
    private String link;

    public HuttssaysViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(link == null || link.isEmpty())
                    return;

                //open link in YouTube
            }
        });
        ButterKnife.bind(this, itemView);
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
