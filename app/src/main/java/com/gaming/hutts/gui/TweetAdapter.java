package com.gaming.hutts.gui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gaming.hutts.huttscentral.R;
import com.gaming.hutts.data.Tweet;

import java.util.ArrayList;

/**
 * Created by Martin on 11. 6. 2016.
 */
public class TweetAdapter extends RecyclerView.Adapter<TweetViewHolder> {

    private ArrayList<Tweet> tweets;

    public TweetAdapter(ArrayList<Tweet> list){
        this.tweets = list;
    }

    @Override
    public TweetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tweet_item, parent, false);
        TweetViewHolder vh = new TweetViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(TweetViewHolder holder, int position) {
        BitmapManager.getInstance().loadBitmap(tweets.get(position).getProfileImageUrl(), holder.profile_pic);
        holder.name.setText(tweets.get(position).getName());
        holder.username.setText(tweets.get(position).getUserName());
        holder.date.setText(tweets.get(position).getDate());
        holder.message.setText(tweets.get(position).getMessage());
    }


    @Override
    public int getItemCount() {
        //returns number of elements
        return tweets.size();
    }
}
