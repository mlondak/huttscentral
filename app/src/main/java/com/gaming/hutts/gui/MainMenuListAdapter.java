package com.gaming.hutts.gui;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gaming.hutts.data.HuttsSays;
import com.gaming.hutts.data.Tweet;
import com.gaming.hutts.huttscentral.R;

import java.util.HashMap;
import java.util.List;

public class MainMenuListAdapter extends BaseExpandableListAdapter {

    private Context _context;

    // header titles
    private List<String> _listDataHeader;

    // data for each header
    private HashMap<String, List<Object>> _listDataChild;

    public MainMenuListAdapter(Context context, List<String> listDataHeader,
                                 HashMap<String, List<Object>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public int getGroupCount() {
        return _listDataHeader.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return _listDataChild.get(_listDataHeader.get(i)).size();
    }

    @Override
    public Object getGroup(int i) {
        return _listDataHeader.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return _listDataChild.get(_listDataHeader.get(i)).get(i1);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {

        //inflate header layout, should be the same for all headers
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) _context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.mainmenu_list_header, null);
        }

        //change title of the header accordingly to the group
        String headerTitle = (String) getGroup(i);
        TextView header_title = (TextView) view.findViewById(R.id.text_header);
        header_title.setText(headerTitle);

        ImageView arrow = (ImageView) view.findViewById(R.id.img_arrow);

        if(!b) //if is not expanded
        {
            arrow.setImageResource(R.drawable.arrow_down);
        }
        else {
            arrow.setImageResource(R.drawable.arrow_up);
        }
        //set alpha to match somewhat transparent color
        //used for header background, which is alpha = AA (170)
        arrow.setAlpha(170f/255f);

        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        Object childObject = getChild(i, i1);

            LayoutInflater inflater = (LayoutInflater) _context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            switch(i)
            {
                case 0:
                    view = inflater.inflate(R.layout.youtube_list_item, null);
                    break;
                case 1:
                    view = inflater.inflate(R.layout.tweet_item, null);
                    break;
                case 2:
                default:
                    view = inflater.inflate(R.layout.huttssays_item, null);
                    break;
            }

        try {

            switch (i) {
                case 0:
                    //nothing here for youtube now
                    break;
                case 1: //fill in Tweet data
                    ImageView profile_pic = (ImageView) view.findViewById(R.id.tweet_item_profile_pic);
                    TextView name = (TextView) view.findViewById(R.id.tweet_item_name);
                    TextView username = (TextView) view.findViewById(R.id.tweet_item_username);
                    TextView date = (TextView) view.findViewById(R.id.tweet_item_date);
                    TextView message = (TextView) view.findViewById(R.id.tweet_item_message);

                    Tweet obj2 = (Tweet) childObject;
                    BitmapManager.getInstance().loadBitmap(obj2.getProfileImageUrl(), profile_pic);
                    name.setText(obj2.getName());
                    username.setText(obj2.getUserName());
                    date.setText(obj2.getDate());
                    message.setText(obj2.getMessage());
                    break;
                case 2: //fill in Huttssays data
                default:
                    TextView title = (TextView) view.findViewById(R.id.hs_title_tv);
                    TextView signature = (TextView) view.findViewById(R.id.hs_signature_tv);

                    HuttsSays obj3 = (HuttsSays) childObject;
                    title.setText(obj3.getTitle());
                    signature.setText(obj3.getSignature());

                    break;
            }
        }
        catch (Exception e) {
            Log.e("MainMenuListAdapter", "Exception while filling object " + i + "-" + i1 +
                    "(group-item). Exception: " + e.getMessage());
        }

        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        if(i == 0)
            return true;

        return false;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
}
