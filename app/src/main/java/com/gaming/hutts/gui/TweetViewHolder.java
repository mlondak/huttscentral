package com.gaming.hutts.gui;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gaming.hutts.huttscentral.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Martin on 12. 6. 2016.
 */
public class TweetViewHolder extends RecyclerView.ViewHolder  {

    @BindView(R.id.tweet_item_profile_pic) ImageView profile_pic;
    @BindView(R.id.tweet_item_name) TextView name;
    @BindView(R.id.tweet_item_username) TextView username;
    @BindView(R.id.tweet_item_date) TextView date;
    @BindView(R.id.tweet_item_message) TextView message;

    public TweetViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
