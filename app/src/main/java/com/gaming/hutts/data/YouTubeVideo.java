package com.gaming.hutts.data;

import com.google.api.client.util.DateTime;

import java.math.BigInteger;

/**
 * Created by Martin on 5. 7. 2016.
 */
public class YouTubeVideo {
    private String title;
    private String videoId;
    private DateTime publishedAt;
    private String duration;
    private BigInteger viewCount;

    public YouTubeVideo(String title, String videoId, DateTime publishedAt, String duration, BigInteger viewCount) {
        this.title = title;
        this.videoId = videoId;
        this.publishedAt = publishedAt;
        this.viewCount = viewCount;

        //format duration
        boolean hasHours = false;
        boolean hasMinutes = false;
        boolean hasSeconds = false;

        if(duration.indexOf('H') >= 0)
            hasHours = true;

        if(duration.indexOf('M') >= 0)
            hasMinutes = true;

        if(duration.indexOf('S') >= 0)
            hasSeconds = true;

        String[] arr = duration.replaceAll("[^0-9]+", " ").trim().split(" ");

        this.duration = String.format("%s%02d:%02d", hasHours ? arr[0] + ":" : "",
                hasMinutes ? hasHours?Integer.parseInt(arr[1]):Integer.parseInt(arr[0]) : 0,
                hasSeconds ? Integer.parseInt(arr[arr.length-1]) : 0);
    }

    public String getTitle() {
        return title;
    }

    public String getVideoId() {
        return videoId;
    }

    public DateTime getPublishedAt() {
        return publishedAt;
    }

    public String getDuration() {
        return duration;
    }

    public BigInteger getViewCount() { return viewCount; }
}
