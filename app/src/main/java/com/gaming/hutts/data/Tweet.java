package com.gaming.hutts.data;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Martin on 11. 6. 2016.
 */
public class Tweet {

    private String id;
    private String name;
    private String userName;
    private String profileImageUrl;
    private String message;
    private String date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name; }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        if(userName.startsWith("@"))
            return userName;
        else
            return "@"+userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        String dataWithoutTimeZone = removeTimeZone(date);
        this.date = formatData(dataWithoutTimeZone);
    }

    private String formatData(String data){
        if(data.contains(" at "))
            return data; //already in correct format

        String strData = null;
        TimeZone tzUTC = TimeZone.getTimeZone("UTC");
        SimpleDateFormat formattedDate = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy", Locale.US);
        formattedDate.setTimeZone(tzUTC);
        SimpleDateFormat tweetDateFormat = new SimpleDateFormat("EEE, dd.MMM ''yy, 'at' HH:mm", Locale.US);

        try {
            strData = tweetDateFormat.format(formattedDate.parse(data));
        } catch (Exception e) {
            Log.e("Error in parsing date", Log.getStackTraceString(e));
        }
        return strData;
    }

    private String removeTimeZone(String data){
        return data.replaceFirst("(\\s[+|-]\\d{4})", "");
    }

    public boolean isMoreThanWeekOld() {

        SimpleDateFormat tweetDateFormat = new SimpleDateFormat("EEE, dd.MMM ''yy, 'at' HH:mm", Locale.US);

        try {
            Date tweetDate = tweetDateFormat.parse(getDate());
            Calendar c = Calendar.getInstance();
            c.setTime(tweetDate);
            //add 7 days to compare it to NOW
            c.add(Calendar.DATE, 7);
            tweetDate = c.getTime();

            Date now = new Date();

            return tweetDate.before(now);
        } catch (Exception e) {
            Log.e("Error in parsing date", Log.getStackTraceString(e));
        }

        return false;
    }
}
