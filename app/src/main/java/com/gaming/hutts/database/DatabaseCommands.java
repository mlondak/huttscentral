package com.gaming.hutts.database;

/**
 * Created by Martin on 19. 6. 2016.
 */
public class DatabaseCommands {
    public static final String SQL_CREATE_TABLE_TWITTER =
            "CREATE TABLE IF NOT EXISTS " + DatabaseContract.TwitterTable.TABLE_NAME + " (" +
                    DatabaseContract.TwitterTable.COLUMN_ID + " INTEGER PRIMARY KEY," +
                    DatabaseContract.TwitterTable.COLUMN_TWEET_ID + " TEXT, " +
                    DatabaseContract.TwitterTable.COLUMN_NAME + " TEXT, " +
                    DatabaseContract.TwitterTable.COLUMN_USERNAME + " TEXT, " +
                    DatabaseContract.TwitterTable.COLUMN_PROFILE_PIC + " TEXT, " +
                    DatabaseContract.TwitterTable.COLUMN_MESSAGE + " TEXT, " +
                    DatabaseContract.TwitterTable.COLUMN_DATE + " TEXT" + " ); "
            ;
    public static final String SQL_CREATE_TABLE_HUTTSSAYS =
            "CREATE TABLE IF NOT EXISTS " + DatabaseContract.HuttssaysTable.TABLE_NAME + " (" +
                    DatabaseContract.HuttssaysTable.COLUMN_ID + " INTEGER PRIMARY KEY, " +
                    DatabaseContract.HuttssaysTable.COLUMN_TITLE + " TEXT, " +
                    DatabaseContract.HuttssaysTable.COLUMN_LINK + " TEXT, " +
                    DatabaseContract.HuttssaysTable.COLUMN_SIG + " TEXT, " +
                    DatabaseContract.HuttssaysTable.COLUMN_URL_ID + " TEXT, " +
                    DatabaseContract.HuttssaysTable.COLUMN_DATE + " TEXT );"
            ;


    public static final String SQL_DELETE_TABLE_TWITTER =
            "DROP TABLE IF EXISTS " +
                    DatabaseContract.TwitterTable.TABLE_NAME + "; ";
    public static final String SQL_DELETE_TABLE_HUTTSSAYS =
            "DROP TABLE IF EXISTS " +
                    DatabaseContract.HuttssaysTable.TABLE_NAME + ";";
}
