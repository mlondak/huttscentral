package com.gaming.hutts.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.gaming.hutts.data.HuttsSays;
import com.gaming.hutts.data.Tweet;
import com.gaming.hutts.huttscentral.HuttsCentralApplication;

import java.util.ArrayList;

/**
 * Created by Martin on 19. 6. 2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    //singleton pattern
    private static DatabaseHelper instance;
    // If the database schema is changed, the database version must be incremented
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "HuttsCentral.db";

    public static synchronized DatabaseHelper getInstance() {
        if(instance == null)
            return new DatabaseHelper();

        return instance;
    }

    private DatabaseHelper() {
        super(HuttsCentralApplication.getAppContext(), DATABASE_NAME, null, DATABASE_VERSION);
        instance = this;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DatabaseCommands.SQL_CREATE_TABLE_TWITTER);
        sqLiteDatabase.execSQL(DatabaseCommands.SQL_CREATE_TABLE_HUTTSSAYS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // This database stores online data only
        // if upgrade or downgrade is detected
        // discard the data and start over
        sqLiteDatabase.execSQL(DatabaseCommands.SQL_DELETE_TABLE_TWITTER);
        sqLiteDatabase.execSQL(DatabaseCommands.SQL_DELETE_TABLE_HUTTSSAYS);
        onCreate(sqLiteDatabase);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public ArrayList<Tweet> getTwitterRows() {
        return getTwitterRows(-1);
    }

    public ArrayList<Tweet> getTwitterRows(int maxRows) {
        SQLiteDatabase db = getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                DatabaseContract.TwitterTable.COLUMN_TWEET_ID,
                DatabaseContract.TwitterTable.COLUMN_NAME,
                DatabaseContract.TwitterTable.COLUMN_USERNAME,
                DatabaseContract.TwitterTable.COLUMN_PROFILE_PIC,
                DatabaseContract.TwitterTable.COLUMN_MESSAGE,
                DatabaseContract.TwitterTable.COLUMN_DATE
        };

        // How you want the results sorted in the resulting Cursor
        String sortOrder = DatabaseContract.TwitterTable.COLUMN_TWEET_ID + " DESC";

        Cursor c = db.query(DatabaseContract.TwitterTable.TABLE_NAME,
                            projection,
                            null, null,
                            null, null,
                            sortOrder,
                            maxRows == -1 ? null : String.valueOf(maxRows));

        if(!c.moveToFirst()) {
            c.close();
            Log.d("HC Database", "No entries read in Twitter table");
            return null; //empty cursor, no data read
        }

        ArrayList<Tweet> list = new ArrayList<Tweet>();
        do {
            Tweet t = new Tweet();
            t.setId(c.getString(c.getColumnIndexOrThrow(DatabaseContract.TwitterTable.COLUMN_TWEET_ID)));
            t.setName(c.getString(c.getColumnIndexOrThrow(DatabaseContract.TwitterTable.COLUMN_NAME)));
            t.setUserName(c.getString(c.getColumnIndexOrThrow(DatabaseContract.TwitterTable.COLUMN_USERNAME)));
            t.setProfileImageUrl(c.getString(c.getColumnIndexOrThrow(DatabaseContract.TwitterTable.COLUMN_PROFILE_PIC)));
            t.setMessage(c.getString(c.getColumnIndexOrThrow(DatabaseContract.TwitterTable.COLUMN_MESSAGE)));
            t.setDate(c.getString(c.getColumnIndexOrThrow(DatabaseContract.TwitterTable.COLUMN_DATE)));
            list.add(t);
        } while(c.moveToNext());

        c.close();
        return list.size() == 0 ? null : list;
    }

    /*
     * This method return the latest tweet in DB
     */
    public Tweet getMostRecentTweet() {
        SQLiteDatabase db = getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                DatabaseContract.TwitterTable.COLUMN_TWEET_ID,
                DatabaseContract.TwitterTable.COLUMN_NAME,
                DatabaseContract.TwitterTable.COLUMN_USERNAME,
                DatabaseContract.TwitterTable.COLUMN_PROFILE_PIC,
                DatabaseContract.TwitterTable.COLUMN_MESSAGE,
                DatabaseContract.TwitterTable.COLUMN_DATE
        };

        // How you want the results sorted in the resulting Cursor
        String sortOrder = DatabaseContract.TwitterTable.COLUMN_TWEET_ID + " DESC";

        Cursor c = db.query(DatabaseContract.TwitterTable.TABLE_NAME,
                projection,
                null, null,
                null, null,
                sortOrder,
                "1");

        if(!c.moveToFirst()) {
            c.close();
            Log.d("HC Database", "No entries read in Twitter table");
            return null; //empty cursor, no data read
        }


        Tweet t = new Tweet();
        t.setId(c.getString(c.getColumnIndexOrThrow(DatabaseContract.TwitterTable.COLUMN_TWEET_ID)));
        t.setName(c.getString(c.getColumnIndexOrThrow(DatabaseContract.TwitterTable.COLUMN_NAME)));
        t.setUserName(c.getString(c.getColumnIndexOrThrow(DatabaseContract.TwitterTable.COLUMN_USERNAME)));
        t.setProfileImageUrl(c.getString(c.getColumnIndexOrThrow(DatabaseContract.TwitterTable.COLUMN_PROFILE_PIC)));
        t.setMessage(c.getString(c.getColumnIndexOrThrow(DatabaseContract.TwitterTable.COLUMN_MESSAGE)));
        t.setDate(c.getString(c.getColumnIndexOrThrow(DatabaseContract.TwitterTable.COLUMN_DATE)));

        c.close();
        return t;
    }

    public boolean doDataExistInDb(String tableName) {
        if(tableName == null || tableName.isEmpty()){
            Log.e("HC Database", "doDataExistInDb - empty or null string as argument");
            throw new IllegalArgumentException("doDataExistInDb - Empty or null string as argument");
        }

        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT COUNT(*) FROM " + tableName;
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        int count = c.getInt(0);
        c.close();

        return count > 0;
    }

    public boolean insertBulkTwitterRows(ArrayList<Tweet> list) {
        if(list == null || list.size() == 0)
            return false;

        try {
            SQLiteDatabase db = getWritableDatabase();
            StringBuilder query = new StringBuilder("INSERT INTO " + DatabaseContract.TwitterTable.TABLE_NAME + " ( " +
                    DatabaseContract.TwitterTable.COLUMN_TWEET_ID + ", " +
                    DatabaseContract.TwitterTable.COLUMN_NAME + ", " +
                    DatabaseContract.TwitterTable.COLUMN_USERNAME + ", " +
                    DatabaseContract.TwitterTable.COLUMN_PROFILE_PIC + ", " +
                    DatabaseContract.TwitterTable.COLUMN_MESSAGE + ", " +
                    DatabaseContract.TwitterTable.COLUMN_DATE + " ) VALUES");
            for (Tweet t : list) {
                query.append(" ( \"" + t.getId() + "\" , \"" + t.getName() + "\" , \"" + t.getUserName() + "\" , \"" +
                        t.getProfileImageUrl() + "\" , \"" + t.getMessage().replace('"', '\'')
                        + "\" , \"" + t.getDate() + "\" ),"
                );
                Log.d("DB insert", "Inserting Tweet with ID: " + t.getId());
            }
            query.deleteCharAt(query.length()-1);
            query.append(';');
            db.execSQL(query.toString());
        } catch(Exception e) {
            Log.e("HC Database", "Error while inserting tweets to DB: " + e);
            return false;
        }

        return true;
    }

    public boolean insertOneTweetRow(Tweet t) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues row = new ContentValues();
        row.put(DatabaseContract.TwitterTable.COLUMN_TWEET_ID, t.getId());
        row.put(DatabaseContract.TwitterTable.COLUMN_NAME, t.getName());
        row.put(DatabaseContract.TwitterTable.COLUMN_USERNAME, t.getUserName());
        row.put(DatabaseContract.TwitterTable.COLUMN_PROFILE_PIC, t.getProfileImageUrl());
        row.put(DatabaseContract.TwitterTable.COLUMN_MESSAGE, t.getMessage().replace('"', '\''));
        row.put(DatabaseContract.TwitterTable.COLUMN_DATE, t.getDate());

        long newRowId = db.insert(DatabaseContract.TwitterTable.TABLE_NAME, null, row);
        if(newRowId >= 0)
            return true;

        return false;
    }

    public ArrayList<HuttsSays> getHuttssaysRows() {
        return getHuttssaysRows(-1);
    }

    public ArrayList<HuttsSays> getHuttssaysRows(int maxRows) {
        SQLiteDatabase db = getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                DatabaseContract.HuttssaysTable.COLUMN_TITLE,
                DatabaseContract.HuttssaysTable.COLUMN_LINK,
                DatabaseContract.HuttssaysTable.COLUMN_SIG,
                DatabaseContract.HuttssaysTable.COLUMN_URL_ID,
                DatabaseContract.HuttssaysTable.COLUMN_DATE
        };

        // How you want the results sorted in the resulting Cursor
        String sortOrder = DatabaseContract.HuttssaysTable.COLUMN_DATE + " ASC";

        Cursor c = db.query(DatabaseContract.HuttssaysTable.TABLE_NAME,
                projection,
                null, null,
                null, null,
                sortOrder,
                maxRows == -1 ? null : String.valueOf(maxRows));

        if(!c.moveToFirst()) {
            c.close();
            Log.d("HC Database", "No entries read in Huttssays table");
            return null; //empty cursor, no data read
        }

        ArrayList<HuttsSays> list = new ArrayList<HuttsSays>();
        do {
            HuttsSays hs = new HuttsSays();
            hs.setTitle(c.getString(c.getColumnIndexOrThrow(DatabaseContract.HuttssaysTable.COLUMN_TITLE)));
            hs.setUrlLink(c.getString(c.getColumnIndexOrThrow(DatabaseContract.HuttssaysTable.COLUMN_LINK)));
            hs.setSignature(c.getString(c.getColumnIndexOrThrow(DatabaseContract.HuttssaysTable.COLUMN_SIG)));
            hs.setWebId(c.getString(c.getColumnIndexOrThrow(DatabaseContract.HuttssaysTable.COLUMN_URL_ID)));
            hs.setDate(c.getString(c.getColumnIndexOrThrow(DatabaseContract.HuttssaysTable.COLUMN_DATE)));
            list.add(hs);
        } while(c.moveToNext());

        c.close();
        return list.size() == 0 ? null : list;
    }

    public boolean insertBulkHuttssaysRows(ArrayList<HuttsSays> list) {
        if(list == null || list.size() == 0)
            return false;

        try {
            SQLiteDatabase db = getWritableDatabase();
            StringBuilder query = new StringBuilder("INSERT INTO " + DatabaseContract.HuttssaysTable.TABLE_NAME +
                    " ( " + DatabaseContract.HuttssaysTable.COLUMN_TITLE + ", " +
                    DatabaseContract.HuttssaysTable.COLUMN_LINK + ", " +
                    DatabaseContract.HuttssaysTable.COLUMN_SIG + ", " +
                    DatabaseContract.HuttssaysTable.COLUMN_URL_ID + ", " +
                    DatabaseContract.HuttssaysTable.COLUMN_DATE + " ) VALUES" );
            for (HuttsSays hs : list) {
                query.append(" ( \"" + hs.getTitle().replace('"', '\'') + "\" , \"" +
                        hs.getUrlLink() + "\" , \"" + hs.getSignature() + "\" , \"" +
                        hs.getWebId() + "\", \"" + hs.getDate() + "\" ),"
                );
            }
            query.deleteCharAt(query.length()-1);
            query.append(';');
            db.execSQL(query.toString());
        } catch(Exception e) {
            Log.e("HC Database", "Error while inserting tweets to DB: " + e);
            return false;
        }

        return true;
    }

    public boolean insertOneHuttssaysRow(HuttsSays hs) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues row = new ContentValues();
        row.put(DatabaseContract.HuttssaysTable.COLUMN_TITLE, hs.getTitle().replace('"', '\''));
        row.put(DatabaseContract.HuttssaysTable.COLUMN_LINK, hs.getUrlLink());
        row.put(DatabaseContract.HuttssaysTable.COLUMN_SIG, hs.getSignature());
        row.put(DatabaseContract.HuttssaysTable.COLUMN_URL_ID, hs.getWebId());
        row.put(DatabaseContract.HuttssaysTable.COLUMN_DATE, hs.getDate());

        long newRowId = db.insert(DatabaseContract.HuttssaysTable.TABLE_NAME, null, row);
        if(newRowId >= 0)
            return true;

        return false;
    }
}
