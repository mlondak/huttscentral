package com.gaming.hutts.database;

import android.provider.BaseColumns;

/**
 * Created by Martin on 19. 6. 2016.
 */
public final class DatabaseContract {
    //empty constructor just in case somebody tried to instantiate this
    public DatabaseContract() {}

    public static abstract class TwitterTable implements BaseColumns {
        public static final String TABLE_NAME = "twitter_table";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_TWEET_ID = "t_id";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_USERNAME = "username";
        public static final String COLUMN_PROFILE_PIC = "profile_pic";
        public static final String COLUMN_MESSAGE = "message";
        public static final String COLUMN_DATE = "date";
    }

    public static abstract class HuttssaysTable implements BaseColumns {
        public static final String TABLE_NAME = "huttssays_table";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_LINK = "url_link";
        public static final String COLUMN_SIG = "signature";
        public static final String COLUMN_URL_ID = "url_id";
        public static final String COLUMN_DATE = "date";
    }
}
